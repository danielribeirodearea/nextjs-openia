"use client"

import { ChatArea } from "@/components/ChatArea";
import { Footer } from "@/components/Footer";
import { Header } from "@/components/Header";
import { SideBarChatButton } from "@/components/SideBarChatButton";
import { Sidebar }  from "@/components/Sidebar";
import { Chat } from "@/types/chat";
import { useEffect, useState } from "react";
import { v4 as uuidv4 } from 'uuid'

const Page = () => {

  const [sideBarOpened, setSideBarOpened] = useState(true)
  const [AILoading, setAILoading] = useState(false)
  const [chatActive, setChatActive] = useState<Chat>()
  const [chatList, setChatList] = useState<Chat[]>([])
  const [chatActiveId, setChatActiveId] = useState<string>('')

  useEffect(() => {

    setChatActive(chatList.find(item => item.id === chatActiveId))

  }, [chatActiveId, chatList])

  const getIAresponse = () => {
    setTimeout(() => {
      let newChatid = uuidv4()
      let chatListClone = [...chatList]
      let chatIndex = chatListClone.findIndex(item => item.id === chatActiveId)
      if(chatIndex > -1){
        chatListClone[chatIndex].messages.push({
          id: newChatid, 
          author: 'ai',
          body: 'Aqui vai a resposta da AI.'
        })
      }
      setChatList(chatListClone)
      setAILoading(false)
    },2000)
  }

  useEffect(() => {
    if(AILoading){
      getIAresponse()
    }
  }, [AILoading])

  const closeSidebar = () => {
    setSideBarOpened(false)
  }
  const openSideBar = () => {
    setSideBarOpened(true)
  }

  const handleClear = () => {
    if(AILoading){
      return
    }

    setChatActiveId('')
    setChatList([])

  }

  const handleNewChat = () => {
    if(AILoading){
      return
    }
    setChatActiveId('')
    closeSidebar()
  }

  const handleSendMessage = (message: string) => {

    if(!chatActiveId){
      let newChatid = uuidv4()
      let newChatMessageId = uuidv4()
      setChatList([{
        id: newChatid,
        title: message,
        messages: [
          {
            id: newChatMessageId,
            author: 'me',
            body: message
          }
        ]
      }, ...chatList])

      setChatActiveId(newChatid)

    }else{
      let newChatid = uuidv4()
      let chatListClone = [...chatList]
      let chatIndex = chatListClone.findIndex(item => item.id === chatActiveId)
      chatListClone[chatIndex].messages.push({
        id: newChatid, 
        author: 'me',
        body: message
      })
      setChatList(chatListClone)

    }

    setAILoading(true)
    
  }

  const handleSelectChat = (id: string) => {
    	if(AILoading){
        return
      }

      let item = chatList.find(item => item.id === id)
      if(item){
        setChatActiveId(item.id)
        closeSidebar()
      }

  }

  const handleDeleteChat = (id: string) => {
    let chatListClone = [...chatList]
    let chatIndex = chatListClone.findIndex(item => item.id === id)
    chatListClone.splice(chatIndex, 1)
    setChatList(chatListClone)
    setChatActiveId('')
  }

  const handleEditChat = (id: string, newTitle: string) => {
    let chatListClone = [...chatList]
    let chatIndex = chatListClone.findIndex(item => item.id === id)
    chatListClone[chatIndex].title = newTitle
    setChatList(chatListClone)
    
  }


  return (
    <main className="flex min-h-screen bg-gpt-gray">
      <Sidebar
        open={sideBarOpened}
        onClose={closeSidebar}
        onClear={handleClear}
        OnNewChat={handleNewChat}
      >
        {chatList.map(item => (
          <SideBarChatButton
            key={item.id}
            chatItem={item}
            active={item.id === chatActiveId}
            onClick={handleSelectChat}
            onDelete={handleDeleteChat}
            onEdit={handleEditChat}
          />
        ))}
      </Sidebar>

      <section className="flex flex-col w-full">
        <Header 
          openSideBarClick={openSideBar}
          title={chatActive ? chatActive.title: 'Nova conversa'}
          newChatClick={handleNewChat}
        />

        <ChatArea
          chat={chatActive}
          loading={AILoading}
        />

        <Footer
          onSendMessage={handleSendMessage}
          disabled={AILoading}
        />
      </section>
    </main>
  )

}

export default Page;