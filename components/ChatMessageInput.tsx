import { useState, KeyboardEvent, useRef, useEffect } from "react"
import IconSend from "./icons/send-icon"

type Props = {
    onSend:  (message: string) => void
    disabled: boolean
}

export const ChatMessageInput = ({onSend, disabled} : Props) => {
    const [text, setText] = useState('')
    const textEl = useRef<HTMLTextAreaElement>(null)

    useEffect(() => {
        if(textEl.current){
            textEl.current.style.height = '0px'
            let scroolHeight = textEl.current.scrollHeight
            textEl.current.style.height = scroolHeight + 'px'
        }
    }, [text, textEl])

    const handleSendMessage = () => {
        if(!disabled && text.trim() !== ''){
            onSend(text)
            setText('')
        }
    }

    const handleTextKeyUp = (event: KeyboardEvent<HTMLTextAreaElement>) => {
        if(event.code.toLowerCase() === 'enter' && !event.shiftKey){
            event.preventDefault()
            handleSendMessage()
        }
    }

    return(
        <div className={`flex border border-gray-800/50 bg-gpt-lightgray p-2 rounded-md ${disabled && 'opacity-50'}`}>
            <textarea className="flex-1 border-0 bg-transparent resize-none outline-none h-6 max-h-48 overflow-auto"
                placeholder="Digite uma mensagem"
                value={text}
                onChange={e => setText(e.target.value)}
                onKeyUp={handleTextKeyUp}
                disabled={disabled}
                ref={textEl}>
            </textarea>
            <div onClick={handleSendMessage} className={`self-end p-1 cursor-point rounded ${text.length ? 'opacity-100 hover:bg-black/20' : 'opacity-20'}`}>
                <IconSend width={14} height={14} />
            </div>
        </div>
    )
}