import { ChatMessages } from "./ChatMessages"

export type Chat = {
    id: string
    title: string
    messages: ChatMessages[]
}