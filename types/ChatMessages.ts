export type ChatMessages = {
    id: string
    author: 'me' | 'ai'
    body: string
}